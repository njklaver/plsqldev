/**
* &<Name = "Purpose">
*
*/
PROCEDURE &<NAME = "Procedure Name" lowercase = "true" >(&<NAME = "Parameters">) IS
  --
  --co_unit Program unit name.
  co_unit CONSTANT sys_public_types.object_name_type := '&<Name = "Procedure Name" lowercase = "true" >';
BEGIN
  /*
  TODO: owner="$OSUSER" category="Finish" priority="1 - High" created="$DATE"
  text="Stubbed: Add functionality"
  */
  [#]
EXCEPTION
  WHEN OTHERS THEN
      utl_log.excep(in_module      => co_package_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
       RAISE;                    
END &<NAME = "Procedure Name" lowercase = "true" >;
