/*
* &<name = "Purpose">
* 
* x_return_status Return status (GC_RET_STS_SUCCESS, GC_RET_STS_ERROR, GC_RET_STS_UNEXP_ERROR).
*
* Return: &<Name = "Return Value"> 
*/
function &<name = "Function Name" lowercase = "true" >(&<name = "Parameters" suffix = ","> x_return_status in out nocopy sys_public_types.t_return_status) return &<Name = "Return Type"> is
  --
  -- x_unit Program unit name.
  x_unit constant sys_public_types.t_object_name := '&<name = "Function Name" lowercase = "true" >';
  --
  -- lx_return_status Local value for <code>X_RETURN_STATUS</code>.
  lx_return_status sys_public_types.t_return_status;
begin
  lx_return_status := x_return_status;
  if lx_return_status  = sys_public_types.gc_ret_sts_unexp_error
  then
    return null;
  end if;
  --
  /*
  TODO: owner="$OSUSER" category="Finish" priority="1 - High" created="$DATE"
  text="Stubbed: Add functionality"
  */
  [#]
  --
  x_return_status := lx_return_status;
  return ;
exception
  when sys_public_types.e_unexpected then
    utl_exceptions.raise_error(sys_public_types.gn_unexpected, package_name, x_unit, true);
    x_return_status := sys_public_types.gc_ret_sts_unexp_error;
    return null;
  when sys_public_types.e_error then
    utl_exceptions.raise_error(sys_public_types.gn_error, package_name, x_unit, true);
    x_return_status := sys_public_types.gc_ret_sts_unexp_error;
    return null;
  when sys_public_types.e_warning then
    utl_exceptions.raise_error(sys_public_types.gn_warning, package_name, x_unit, true);
    x_return_status := sys_public_types.gc_ret_sts_warning;
    return null;
  when sys_public_types.e_fatal then
    raise;
  when others then
    if sqlcode = -20030
    then
      raise sys_public_types.e_fatal;
    end if;
    utl_msg_util.oracle_error(sqlerrm, x_unit, package_name);
    utl_exceptions.raise_error(sys_public_types.gn_unexpected, package_name, x_unit, true);
    x_return_status := sys_public_types.gc_ret_sts_unexp_error;
    return null;
end &<name = "Function Name" lowercase = "true" >;
