/*
* &<name = "Purpose">
*/
PROCEDURE &< NAME = "Procedure Name" lowercase = "true" > (&< NAME = "Parameters" >) IS
  --
  -- co_scope Logger scope
  co_scope CONSTANT logger_logs.scope%type :=  co_scope_prefix || '&<Name = "Procedure Name" lowercase = "true" >';
  --
  -- lt_params Logger parameters.
  lt_params logger.tab_param;
BEGIN
  logger.log(p_text => 'START', p_params => lt_params, p_scope => co_scope);
  --
  /*
  TODO: owner="$OSUSER" category="Finish" priority="1 - High" created="$DATE"
  text="Stubbed: Add functionality"
  */
  [ # ]
  --
  logger.log(p_text => 'END', p_scope => co_scope);
EXCEPTION
  WHEN OTHERS THEN
       logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope,  p_params => lt_params);
       RAISE;                    
END &< NAME = "Procedure Name" lowercase = "true" >;
