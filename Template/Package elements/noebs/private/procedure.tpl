/*
* &<Name = "Purpose">
*
* Raises sys_public_types.e_unexpected when an unexpected error is raised.
* Raises sys_public_types.e_error when an (user) error is raised.
* Raises sys_public_types.e_warning when a warning is raised.
* Raises sys_public_types.e_fatal when a fatal error is raised.
*/
procedure &<name = "Procedure Name" lowercase = "true" >(&<name = "Parameters">) is
  --
  -- x_unit Program unit name.
  x_unit constant sys_public_types.t_object_name := '&<Name = "Procedure Name" lowercase = "true" >';
begin
  /*
  TODO: owner="$OSUSER" category="Finish" priority="1 - High" created="$DATE"
  text="Stubbed: Add functionality"
  */
  [#]
exception
  when sys_public_types.e_unexpected then
    utl_exceptions.raise_exception(sys_public_types.gn_unexpected, package_name, x_unit, true);
  when sys_public_types.e_error then
    utl_exceptions.raise_exception(sys_public_types.gn_error, package_name, x_unit, true);
  when sys_public_types.e_warning then
    utl_exceptions.raise_exception(sys_public_types.gn_warning, package_name, x_unit, true);
  when sys_public_types.e_fatal then
    raise;
  when others then
    if sqlcode = -20030
    then
      raise sys_public_types.e_fatal;
    end if;
    utl_msg_util.oracle_error(sqlerrm, x_unit, package_name);
    utl_exceptions.raise_exception(sys_public_types.gn_unexpected, package_name, x_unit, true);
end &<name = "Procedure Name" lowercase = "true" >;
