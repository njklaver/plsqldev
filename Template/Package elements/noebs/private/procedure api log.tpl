/*
* &<name = "Purpose">
*
* x_return_status Return status (GC_RET_STS_SUCCESS, GC_RET_STS_ERROR or GC_RET_STS_UNEXP_ERROR).
*/
procedure &<name = "Procedure Name" lowercase = "true" >(&<name = "Parameters" Suffix = ","> x_return_status in out nocopy sys_public_types.t_return_status) is
  --
  -- x_unit Program unit name.
  x_unit constant sys_public_types.t_object_name := '&<Name = "Procedure Name" lowercase = "true" >';
  --
  -- lx_return_status Local value for X_RETURN_STATUS.
  lx_return_status sys_public_types.t_return_status;
begin
  lx_return_status := x_return_status;
  if lx_return_status  = sys_public_types.gc_ret_sts_unexp_error
  then
    return;
  end if;
  --
  utl_log.open_begin(package_name, x_unit);
  utl_log.close_entry;
  --
  /*
  TODO: owner="$OSUSER" category="Finish" priority="1 - High" created="$DATE"
  text="Stubbed: Add functionality"
  */
  [#]
  --
  utl_log.open_end;
  utl_log.close_entry;
  --
  x_return_status := lx_return_status;
exception
  when sys_public_types.e_unexpected then
    utl_exceptions.raise_error(sys_public_types.gn_unexpected, package_name, x_unit);
    x_return_status := sys_public_types.gc_ret_sts_unexp_error;
  when sys_public_types.e_error then
    utl_exceptions.raise_error(sys_public_types.gn_error, package_name, x_unit);
    x_return_status := sys_public_types.gc_ret_sts_unexp_error;
  when sys_public_types.e_warning then
    utl_exceptions.raise_error(sys_public_types.gn_warning, package_name, x_unit);
    x_return_status := sys_public_types.gc_ret_sts_warning;
  when sys_public_types.e_fatal then
    raise;
  when others then
    if sqlcode = -20030
    then
      raise sys_public_types.e_fatal;
    end if;
    utl_msg_util.oracle_error(sqlerrm, x_unit, package_name);
    utl_exceptions.raise_error(sys_public_types.gn_unexpected, package_name, x_unit);
    x_return_status := sys_public_types.gc_ret_sts_unexp_error;
end &<name = "Procedure Name" lowercase = "true" >;
