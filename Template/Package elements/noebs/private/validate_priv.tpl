  /*
  * Validates the &<name="Comment"> supplied.
  *
  * in_&<name ="PK" lowercase = "true"> &<name="Comment"> to handle (PK non translatable).
  * io_&<name="Alias" lowercase = "true">_rv &<name="Comment"> record to handle.
  */
  PROCEDURE validate_priv(in_&<name ="PK" lowercase = "true"> IN NUMBER,
                          io_&<name="Alias" lowercase = "true">_rv IN OUT NOCOPY &<name = "Table" lowercase = "true">_v%ROWTYPE) IS
    --
    -- co_unit Program unit name. Used in logging.
    co_unit CONSTANT sys_public_types.object_name_type := 'validate_priv';
  BEGIN
    utl_log.enter(in_module => co_package_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'in_&<name ="PK" lowercase = "true">', in_value => in_&<name ="PK" lowercase = "true">);
    utl_log.close_entry;
    --
    utl_error.init(in_master_type => '&<name = "Master Type" uppercase = "true">', in_master_pk => in_&<name ="PK" lowercase = "true">);
    io_jdef_rv := utl_job_definitions_tapi.find_jdef(in_&<name="PK" lowercase="true"> => in_&<name="PK" lowercase="true">);
    --
    --
    &<name = "Table" lowercase = "true">_tapi.set_status(in_&<name="PK" lowercase="true"> => in_&<name="PK" lowercase="true">, in_status => io_&<name = "Alias" lowercase = "true">_rv.status);
    utl_error.flush;
    --
    utl_log.leave(in_module => co_package_name, in_unit => co_unit);
    &<name = "Table" lowercase = "true">_tapi.append_par(in_&<name="Alias" lowercase = "true">_rv => io_&<name="Alias" lowercase = "true">_rv); 	
    utl_log.close_entry;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_package_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END validate_priv;
