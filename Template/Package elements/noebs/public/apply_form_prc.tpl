  /**
  * APEX &<name = "comment"> form apply procedure . 
  *
  * @param in_page_id Current Apex page.
  * @param in_request Apex request.
  *                   {*} CREATE Create a new value set.
  *                   {*} SAVE   Update an existing value set.
  * @param in_&<name = "alias" lowercase = "true">_rv &<name = "comment"> to handle.
  */
  PROCEDURE apply_&<name = "alias" lowercase = "true">_form(in_page_id IN NUMBER,
                          in_request IN sys_public_types.code_type,
                          in_&<name = "alias" lowercase = "true">_rv   IN &<name = "Table" lowercase = true>_v%ROWTYPE,
                          out_&<name = "alias" lowercase = "true">_id out nocopy number) IS
    --
    -- co_unit Program unit name. Used in logging.
    co_unit CONSTANT sys_public_types.object_name_type := 'apply_&<name = "alias" lowercase = "true">_form';
    --
    -- r_&<name = "alias" lowercase = "true">_vl &<name = "comment"> applied.
    -- r_&<name = "alias" lowercase = "true">_rv Updatable copy of in_&<name = "alias" lowercase = "true">_rv.
    r_&<name = "alias" lowercase = "true">_vl    &<name = "Table" lowercase = true>_vl%ROWTYPE;
    r_&<name = "alias" lowercase = "true">_rv    &<name = "Table" lowercase = true>_v%ROWTYPE;
  BEGIN
    utl_log.enter(in_module => co_package_name, in_unit => co_unit);
    &<name = "Table" lowercase = true>_tapi.append_par(in_&<name = "alias" lowercase = "true">_rv => in_&<name = "alias" lowercase = "true">_rv);
    utl_log.set_par('in_request', in_request);
    utl_log.set_par('in_page_id', in_page_id);
    utl_log.close_entry;
    --
    r_&<name = "alias" lowercase = "true">_rv := in_&<name = "alias" lowercase = "true">_rv;
    --
    IF in_request <> 'DELETE'
    THEN
      --
      complete(io_&<name = "alias" lowercase = "true">_rv => r_&<name = "alias" lowercase = "true">_rv);
    END IF;
    --
    CASE in_request
      WHEN 'CREATE' THEN
        r_&<name = "alias" lowercase = "true">_vl := &<name = "Table" lowercase = true>_tapi.ins_&<name = "alias" lowercase = "true">(in_&<name = "alias" lowercase = "true">_rv => r_&<name = "alias" lowercase = "true">_rv);
        --
        apex_util.set_session_state(p_name   => utl_apex_util.item_name(in_item_name => 'ID', in_page_id => in_page_id),
                                    p_value  => r_&<name = "alias" lowercase = "true">_vl.id,
                                    p_commit => FALSE);
        apex_util.set_session_state(p_name   => utl_apex_util.item_name(in_item_name => '&<name = "pk2" uppercase = "true">', in_page_id => in_page_id),
                                    p_value  => r_&<name = "alias" lowercase = "true">_vl.&<name = "pk2" uppercase = "true">,
                                    p_commit => FALSE);
        apex_util.set_session_state(p_name   => utl_apex_util.item_name(in_item_name => 'ROW_VERSION', in_page_id => in_page_id),
                                    p_value  => r_&<name = "alias" lowercase = "true">_vl.row_version,
                                    p_commit => FALSE);
         out_&<name = "alias" lowercase = "true">_id := r_&<name = "alias" lowercase = "true">_vl.out_&<name = "alias" lowercase = "true">_id;
      WHEN 'SAVE' THEN
        r_&<name = "alias" lowercase = "true">_vl := &<name = "Table" lowercase = true>_tapi.upd_&<name = "alias" lowercase = "true">(in_&<name = "alias" lowercase = "true">_rv => r_&<name = "alias" lowercase = "true">_rv);
        --
         out_&<name = "alias" lowercase = "true">_id := r_&<name = "alias" lowercase = "true">_vl.out_&<name = "alias" lowercase = "true">_id;
      WHEN 'DELETE' THEN
        &<name = "Table" lowercase = true>_tapi.del_&<name = "alias" lowercase = "true">(in_&<name = "alias" lowercase = "true">_rv => in_&<name = "alias" lowercase = "true">_rv);
    END CASE;
    --
    utl_log.leave(in_module => co_package_name, in_unit => co_unit);
    &<name = "Table" lowercase = true>_tapi.append_par(in_&<name = "alias" lowercase = "true">rec => r_&<name = "alias" lowercase = "true">_vl);
    utl_log.close_entry;
    utl_log.flush;
  EXCEPTION
    WHEN sys_public_types.e_validation_failed THEN
      utl_log.leave(in_module => co_package_name, in_unit => co_unit);
      utl_log.set_par(in_name => 'exception', in_value => 'sys_public_types.e_validation_failed');
      utl_log.close_entry;
    WHEN sys_public_types.e_update_protection THEN
      utl_log.leave(in_module => co_package_name, in_unit => co_unit);
      utl_log.set_par('exception', 'sys_public_types.e_update_protection');
      utl_log.close_entry;
      utl_log.flush;
      utl_msg.update_protection;
    WHEN OTHERS THEN
      utl_msg.apex_unhandled(in_module     => co_package_name,
                             in_unit       => co_unit,
                             in_errmsg     => SQLERRM,
                             in_sqlcode    => SQLCODE,
                             in_back_trace => sys.dbms_utility.format_error_backtrace);
      utl_log.leave(in_module => co_package_name, in_unit => co_unit);
      utl_log.set_par('exception', 'unhandled');
      utl_log.close_entry;
      utl_log.flush;
  END apply_&<name = "alias" lowercase = "true">_form;
