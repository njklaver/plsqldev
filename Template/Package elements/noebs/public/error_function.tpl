   /**
   * Sets a &<Name = "Error" uppercase = "TRUE"> message.
   *
   *
   * @return &<Name = "Error" uppercase = "TRUE"> message 
   */
   function &<Name = "Error" uppercase = "TRUE"> return sys_public_types.text_type is
      --
      -- co_scope    Logger scope
      -- co_msg_code Message code
      co_scope    constant sys_public_types.logger_scope_type := co_scope_prefix || '&<Name = "Error" uppercase = "TRUE">';
      co_msg_code constant sys_public_types.code_type := '&<Name = "Prefix" uppercase = "TRUE">_&<Name = "Error" uppercase = "TRUE">';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      sys_msg_api.set_name(in_msg_code => co_msg_code);
      --
      return sys_msg_api.get();
   exception
      when others then
         logger.log_error(p_text => sys_log_constants.co_exception, p_scope => co_scope, p_params => lt_params);
         raise;
   end &<Name = "Error" uppercase = "TRUE">;
