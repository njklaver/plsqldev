  /**
  * Cancels the given &<name = "comment" >
  *
  * @param in_id &<name = "comment"> to cancel.
  */
  PROCEDURE cancel_&<name = "alias" lowercase = "true" >(in_id IN INTEGER) IS
    --
    -- co_scope Logger scope
    co_scope CONSTANT logger_logs.scope%TYPE := co_scope_prefix || 'cancel';
    --
    -- lt_params Logger parameters.
    lt_params logger.tab_param;
  BEGIN
    logger.append_param(p_params => lt_params, p_name => 'in_id', p_val => in_id);
    logger.log(p_text => 'START', p_params => lt_params, p_scope => co_scope);
    --
    utl_dml_events_api.set_event(in_det_code => co_&<name = "alias" lowercase = "true" >, in_operation => utl_dml_events_api.co_operation_delete);
    cancel_&<name = "alias" lowercase = "true" >_internal(in_id => in_id, in_cancel_b => TRUE);
    --
    logger.log(p_text => 'END', p_scope => co_scope);
  EXCEPTION
    WHEN OTHERS THEN
      logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
      RAISE;
  END cancel_&<name = "alias" lowercase = "true" >;
