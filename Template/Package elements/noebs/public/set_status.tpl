  /**
  * Sets the status for the &<name="Comment"> supplied.
  *
  * @param in_&<name="PK" lowercase="true"> &<name="Comment"> to handle (PK non translatable).
  * @param in_status       Status to set.
  *                        {*} OK       Ok
  *                        {*} INVALID  Invalid.
  */
  PROCEDURE set_status(in_&<name="PK" lowercase="true"> IN NUMBER,
                       in_status       IN sys_public_types.code_type) IS
    --
    -- co_unit Program unit name. Used in logging.
    co_unit CONSTANT sys_public_types.object_name_type := 'set_status';
  BEGIN
    utl_log.enter(in_module => co_package_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'in_&<name="PK" lowercase="true">', in_value => in_&<name="PK" lowercase="true">);
    utl_log.set_par(in_name => 'in_status', in_value => in_status);
    utl_log.close_entry;
    --
    UPDATE &<name="Table" lowercase="true"> vs
    SET    vs.status = in_status
    WHERE  vs.id = in_&<name="PK" lowercase="true">;
    --
    utl_log.leave(in_module => co_package_name, in_unit => co_unit);
    utl_log.close_entry;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_package_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END set_status;
