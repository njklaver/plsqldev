  /**
  * Returns a new  &<name = "table" uppercase = "false" >_v record.
  *
  *
  * @return  &<name = "table" uppercase = "false" >_v record created.
  */
  FUNCTION &<name = "alias" uppercase = "false" >() RETURN &<name = "table" uppercase = "false" >_v%ROWTYPE IS
    --
    -- lr_&<name = "alias" uppercase = "false" > The &<name = "table" uppercase = "false" >_v record to create.
    lr_&<name = "alias" uppercase = "false" > &<name = "table" uppercase = "false" >_v%rowtype;
  BEGIN
    --
    RETURN lr_&<name = "alias" uppercase = "false" >;
  END &<name = "alias" uppercase = "false" >;
