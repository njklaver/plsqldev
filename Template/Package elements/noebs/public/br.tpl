   /**
   * Business rule &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "BR Name">: &<Name = "BR Desc">
   *
   * @param in_&<name = "alias" uppercase = "false" >           Application to handle
   * @param in_dml_operation DML operation (C, U, D) 
   * @param in_page_id       APEX page
   */
   procedure &<Name = "BR Name">(in_&<name = "alias" uppercase = "false" >           in &<Name = "Prefix" lowercase = "true" required = "Yes">_&<name = "alias" uppercase = "false" >lications_v%rowtype
                   ,in_dml_operation in varchar2
                   ,in_page_id       in number) is
      --
      -- co_scope     Logger scope
      -- co_rule_code Business rule code
      -- co_item_name Associated APEX item (without P<page_id>)
      co_scope     constant logger_logs.scope%type := co_scope_prefix || 'app_01';
      co_rule_code constant &<Name = "Prefix" lowercase = "true" required = "Yes">_public_types.code_type := '&<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "BR Name">';
      co_item_name constant &<Name = "Prefix" lowercase = "true" required = "Yes">_public_types.apex_name_type := 'IND_MAIN';
      --
      --
      -- lt_params   Logger parameters.
      -- l_item_name Associated item name
      lt_params   logger.tab_param;
      l_item_name &<Name = "Prefix" lowercase = "true" required = "Yes">_public_types.apex_name_type;
   begin
      &<Name = "Prefix" lowercase = "true" required = "Yes">_&<name = "alias" uppercase = "false" >lications_tapi.append_par(io_params => lt_params, in_&<name = "alias" uppercase = "false" > => in_&<name = "alias" uppercase = "false" >);
      logger.append_param(p_params => lt_params, p_name => &<Name = "Prefix" lowercase = "true" required = "Yes">_log_constants.co_in_page_id, p_val => in_page_id);
      logger.log(p_text => &<Name = "Prefix" lowercase = "true" required = "Yes">_log_constants.co_start, p_params => lt_params, p_scope => co_scope);
      --
      -- NoFormat start
      l_item_name := case when in_page_id is not null then &<Name = "Prefix" lowercase = "true" required = "Yes">_apex_util.item_name(in_page_id => in_page_id, in_item_name => co_item_name) else null end;
      -- NoFormat end
      &<Name = "Prefix" lowercase = "true" required = "Yes">_msg_api.set_name(in_msg_code => co_rule_code, in_item_name => l_item_name);
      &<Name = "Prefix" lowercase = "true" required = "Yes">_message_api.ins(in_msg_info => &<Name = "Prefix" lowercase = "true" required = "Yes">_msg_api.get_encoded);
      --
      logger.log(p_text => &<Name = "Prefix" lowercase = "true" required = "Yes">_log_constants.co_end, p_scope => co_scope);
   exception
      when others then
         logger.log_error(p_text => &<Name = "Prefix" lowercase = "true" required = "Yes">_log_constants.co_exception, p_scope => co_scope, p_params => lt_params);
         raise;
   end &<Name = "BR Name">;
