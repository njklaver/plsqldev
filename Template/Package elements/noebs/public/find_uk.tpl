  /**
  * Finds a &<name = "Comment"> by CODE (case insenitive).
  *
  * @param in_code &<name = "Comment"> code to be found.
  *
  * @return The &<name = "Comment"> found. 
  */
  FUNCTION find_&<name = "Alias" lowercase = "true">(in_code IN sys_public_types.code_type) RETURN &<name = "Table" lowercase = "true">_v%ROWTYPE IS
    --
    -- co_unit Program unit name. Used in logging.
    co_unit CONSTANT sys_public_types.object_name_type := 'find_&<name = "Alias" lowercase = "true">';
    --
    -- r_&<name = "Alias" lowercase = "true"> Record found.
    r_&<name = "Alias" lowercase = "true">_rv &<name = "Table" lowercase = "true">_v%ROWTYPE;
  BEGIN
    utl_log.enter(in_module => co_package_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'in_code', in_value => in_code);
    utl_log.close_entry;
    --
    <<find>>
    BEGIN
      SELECT &<name = "Alias" lowercase = "true">.
      INTO   r_&<name = "Alias" lowercase = "true">_rv
      FROM   &<name = "Table" lowercase = "true">_v &<name = "Alias" lowercase = "true">
      WHERE  &<name = "Alias" lowercase = "true">.code = upper(in_code);
    EXCEPTION
      WHEN no_data_found THEN
        NULL; -- Just return an empty record.
    END find;
    --
    utl_log.leave(in_module => co_package_name, in_unit => co_unit);
    append_par(in_&<name = "Alias" lowercase = "true">_rv => r_&<name = "Alias" lowercase = "true">_rv);
    utl_log.close_entry;
    --
    RETURN r_&<name = "Alias" lowercase = "true">_rv;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_package_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END find_&<name = "Alias" lowercase = "true">;
