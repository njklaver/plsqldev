  /*
  * Inserts (DML event method a new &<name = "comment">.
  *
  * in_&<name = "alias" lowercase = "true" > Record to handle.
  *
  * Return Record created (current language).
  */
  FUNCTION ins_&<name = "alias" lowercase = "true" >(in_&<name = "alias" lowercase = "true" > IN &<name = "table" lowercase = "true" >_v%ROWTYPE) RETURN &<name = "table" lowercase = "true" >_v%ROWTYPE IS
    --
    -- co_scope Logger scope
    co_scope CONSTANT logger_logs.scope%TYPE := co_scope_prefix || 'ins_&<name = "alias" lowercase = "true" >';
    --
    -- lt_params Logger parameters.
    lt_params logger.tab_param;
    --
    -- lr_&<name = "alias" lowercase = "true" >    Record to handle
    lr_&<name = "alias" lowercase = "true" > &<name = "table" lowercase = "true" >_v%ROWTYPE;
  BEGIN
    &<name = "tapi" lowercase = "true" >.append_par(io_params => lt_params, in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >);
    logger.log(p_text => 'START', p_params => lt_params, p_scope => co_scope);
    --
    lr_&<name = "alias" lowercase = "true" > := &<name = "tapi" lowercase = "true" >.ins_&<name = "alias" lowercase = "true" >(in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >, in_&<name = "alias" lowercase = "true" >_id_old => NULL);
    --
    logger.log(p_text => 'END', p_scope => co_scope);
    --
    RETURN lr_&<name = "alias" lowercase = "true" >;
  EXCEPTION
    WHEN OTHERS THEN
      logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
      RAISE;
  END ins_&<name = "alias" lowercase = "true" >;
