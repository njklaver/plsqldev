/**
* Deletes a &<name = "comment"> from the &<name = "comment"> tables (&<name = "table" lowercase = "true" > and &<name = "table" lowercase = "true" >_TL).
*
* @param in_&<name = "alias" lowercase = "true" > Record to handle.
*/
PROCEDURE del_&<name = "alias" lowercase = "true" >(in_&<name = "alias" lowercase = "true" > IN &<name = "table" lowercase = "true" >_v%ROWTYPE) IS
  --
  -- co_scope Logger scope
  co_scope CONSTANT logger_logs.scope%type :=  co_scope_prefix || 'upd_&<name = "alias" lowercase = "true" >';
  --
  -- lt_params Logger parameters.
  lt_params logger.tab_param;
BEGIN
  SAVEPOINT sp$&<name = "alias" lowercase = "true" >;
  append_par(io_params => lt_params, in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >);
  logger.log(p_text => 'START', p_params => lt_params, p_scope => co_scope);
  --
  DELETE FROM &<name = "table" lowercase = "true" >
  WHERE  id = in_&<name = "alias" lowercase = "true" >.&<name = "alias" lowercase = "true" >_id;
  --
  logger.log(p_text => 'END', p_scope => co_scope);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK TO sp$&<name = "alias" lowercase = "true" >;
    logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope,  p_params => lt_params);
    RAISE;
END del_&<name = "alias" lowercase = "true" >;
