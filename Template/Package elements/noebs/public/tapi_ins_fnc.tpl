  /**
  * Inserts  a new &<name="comment"> in the &<name="comment"> tables (&<name = "table" lowercase = "true" > and &<name = "table" lowercase = "true" >_TL).
  *
  * @param in_&<name = "alias" lowercase = "true" >_rv Record to handle.
  * @param in_&<name = "alias" lowercase = "true" >_id_old The &<name = "comment"> which is updated using DML events. 
  *
  * @return Record created (current language).
  */
FUNCTION ins_&<name = "alias" lowercase = "true" >(in_&<name = "alias" lowercase = "true" > IN &<name = "table" lowercase = "true" >_v%ROWTYPE) RETURN &<name = "table" lowercase = "true" >_v%ROWTYPE IS
   --
   -- co_scope Logger scope
   co_scope CONSTANT logger_logs.scope%type :=  co_scope_prefix || 'ins_&<name = "alias" lowercase = "true" >';
   --
   -- lt_params Logger parameters.
   -- lr_&<name = "alias" lowercase = "true" > Record to insert
   lt_params logger.tab_param;
   lr_&<name = "alias" lowercase = "true" > &<name = "table" lowercase = "true" >_v%ROWTYPE;
BEGIN
   append_par(io_params => lt_params, in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >);
   logger.log(p_text => 'START', p_params => lt_params, p_scope => co_scope);
   --
   utl_dml_events_api.set_event(in_det_code => co_&<name = "alias" lowercase = "true" >, in_operation => utl_dml_events_api.co_operation_create);    
   --
   lr_&<name = "alias" lowercase = "true" > := ins_&<name = "alias" lowercase = "true" >_internal(in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >, in_&<name = "alias" lowercase = "true" >_id_old => NULL, in_ins_b => TRUE, in_ins_tl => TRUE);
  --
   lt_params.delete;
   append_par(io_params => lt_params, in_ &<name = "alias" lowercase = "true" > => lr_&<name = "alias" lowercase = "true" >);
   logger.log(p_text => 'END', p_params => lt_params, p_scope => co_scope);
   --
   RETURN lr_&<name = "alias" lowercase = "true" >;
EXCEPTION
   WHEN OTHERS THEN
      logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope,  p_params => lt_params);
      RAISE;                    
END ins_&<name = "alias" lowercase = "true" >;
  