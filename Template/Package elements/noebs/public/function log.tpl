/**
* &<name = "Purpose">
*
* @return &<Name = "Return Value"> 
*/
function &<name = "Function Name" lowercase = "true" >(&<name = "Parameters">) return &<Name = "Return Type"> is
  --
  -- co_scope Logger scope
  co_scope constant logger_logs.scope%type :=  co_scope_prefix || '&<Name = "Function Name" lowercase = "true" >';
  --
  -- lt_params Logger parameters.
  -- l_retval  Return value
  lt_params logger.tab_param;
  l_retval  &<Name = "Return Type">;
begin
  logger.log(p_text => sys_log_constants.co_start, p_params => lt_params, p_scope => co_scope);
  --
  --
  lt_params.delete;
  logger.append_param(p_params => io_params, p_name => 'l_retval', p_val => l_retval);
  logger.log(p_text => sys_log_constants.co_end, p_scope => co_scope,  p_params => lt_params);
  --
  return ;
exception
  when others then
     logger.log_error(p_text => sys_log_constants.co_exception, p_scope => co_scope,  p_params => lt_params);
     raise;                    
end &<name = "Function Name" lowercase = "true" >;
