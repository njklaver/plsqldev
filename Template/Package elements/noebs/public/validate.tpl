  /**
  * Validates all the &<name="Comment">s in the list.
  *
  * @param in_&<name="Alias" lowercase="true">_list List to handle.
  */
  PROCEDURE validate_bulk(in_&<name="Alias" lowercase="true">_list sys_public_types.text_type) IS
    --
    --co_unit Program unit name.
    co_unit CONSTANT sys_public_types.object_name_type := 'validate_bulk';
    --
    -- t_&<name="PK" lowercase = "true"> List to handle.
    t_&<name="PK" lowercase = "true"> apex_t_number;
    r_dummy        &<name="Table" lowercase="true">_v%ROWTYPE;
  BEGIN
    t_&<name="PK" lowercase = "true"> := apex_string.split_numbers(p_str => in_&<name="Alias" lowercase="true">_list, p_sep => ',');
    FOR idx IN 1 .. t_&<name="PK" lowercase = "true">.count
    LOOP
      validate_priv(in_&<name="PK" lowercase = "true"> => t_&<name="PK" lowercase = "true">(idx), io_&<name="Alias" lowercase="true">_rv => r_dummy);
    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_package_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END validate_bulk;
  /**
  * Validates the &<name="Comment"> supplied
  *
  * @param in_&<name="PK" lowercase = "true"> &<name="Comment"> to handle (PK non translatable).
  *
  * @return The URL for the error page. 
  */
  FUNCTION validate(in_&<name="PK" lowercase = "true"> IN NUMBER) RETURN sys_public_types.text_type IS
    --
    -- co_unit Program unit name. Used for logging.
    co_unit CONSTANT sys_public_types.object_name_type := 'validate';
    --
    -- r_&<name="Alias" lowercase = "true">_rv &<name="Comment"> to handle.
    r_&<name="Alias" lowercase = "true">_rv &<name="Table" lowercase="true">_v%ROWTYPE;
    l_url   sys_public_types.text_type;
  BEGIN
    validate_priv(in_&<name="PK" lowercase = "true"> => in_&<name="PK" lowercase = "true">, io_&<name="Alias" lowercase = "true">_rv => r_&<name="Alias" lowercase = "true">_rv);
    --
    IF r_&<name="Alias" lowercase = "true">_rv.status = 'INVALID'
    THEN
      l_url := utl_error.error_url(in_master_type => '&<name="Master Type" uppercase = "true">', in_master_pk => in_&<name="PK" lowercase = "true">);
    END IF;
    --
    RETURN l_url;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_package_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END validate;
  /**
  * Validates the &<name="Comment"> supplied
  *
  * @param in_&<name="PK" lowercase = "true"> &<name="Comment"> to handle (PK-not translatable).
  *
  * @return TRUE when the &<name="Comment"> supplied is valid.
  */
  FUNCTION validate_bool(in_&<name="PK" lowercase = "true"> IN NUMBER) RETURN BOOLEAN IS
    --
    -- co_unit Program unit name. Used for logging.
    co_unit CONSTANT sys_public_types.object_name_type := 'validate_bool';
    --
    -- r_&<name="Alias" lowercase = "true">_rv &<name="Comment"> to handle.
    r_&<name="Alias" lowercase = "true">_rv &<name="Table" lowercase="true">_v%ROWTYPE;
  BEGIN
    validate_priv(in_&<name="PK" lowercase = "true"> => in_&<name="PK" lowercase = "true">, io_&<name="Alias" lowercase = "true">_rv => r_&<name="Alias" lowercase = "true">_rv);
    --
    RETURN r_&<name="Alias" lowercase = "true">_rv.status = 'OK';
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_package_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END validate_bool;
