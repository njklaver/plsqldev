  /**
  * Updates (DML event method) the  &<name = "comment"> supplied.
  *
  * @param in_&<name = "alias" lowercase = "true" > Record to handle.
  *
  * @return Record updated (current language).
  */
  function upd_&<name = "alias" lowercase = "true" >(in_&<name = "alias" lowercase = "true" > in &<name = "table" lowercase = "true" >_v%rowtype) return &<name = "table" lowercase = "true" >_v%rowtype is
    --
    -- co_scope Logger scope
    co_scope constant logger_logs.scope%type :=  co_scope_prefix || 'upd_&<name = "alias" lowercase = "true" >';
    --
    --
    -- lt_params    Logger parameters.
    -- lr_&<name = "alias" lowercase = "true" >       Record to handle
    -- lr_&<name = "alias" lowercase = "true" >_db    Current values in the database.
    -- l_b          TRUE when the base record must be inserted.
    -- l_tl         TRUE when the TL record must be inserted.
    -- l_id_old     The DMRS project which is updated using DML events (TL record).
    -- l_&<name = "alias" lowercase = "true" >_id_old The DMRS project which is updated using DML events (base record).
    lt_params logger.tab_param;
    lr_&<name = "alias" lowercase = "true" >       &<name = "table" lowercase = "true" >_v%rowtype;
    lr_&<name = "alias" lowercase = "true" >_db    &<name = "table" lowercase = "true" >_v%rowtype;
    l_b          boolean;
    l_tl         boolean;
    l_id_old     &<name = "table" lowercase = "true" >_v.id%type;
    l_&<name = "alias" lowercase = "true" >_id_old &<name = "table" lowercase = "true" >_v.&<name = "alias" lowercase = "true" >_id%type;
  begin
    append_par(io_params => lt_params, in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >);
    logger.log(p_text => 'START', p_params => lt_params, p_scope => co_scope);
    --
    <<check_lost_update>>
    begin
       select &<name = "alias" lowercase = "true" >.*
       into   lr_&<name = "alias" lowercase = "true" >_db
       from   &<name = "gview" lowercase = "true" > &<name = "alias" lowercase = "true" >
       where  &<name = "alias" lowercase = "true" >.id = in_&<name = "alias" lowercase = "true" >.id;
       --
       store_&<name = "alias" lowercase = "true" >(in_ctx => utl_keyval_api.co_db_ctx, in_&<name = "alias" lowercase = "true" > => lr_&<name = "alias" lowercase = "true" >_db);
       store_&<name = "alias" lowercase = "true" >(in_ctx => utl_keyval_api.co_apex_ctx, in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >);
    exception
       when no_data_found then
          raise sys_public_types.e_update_protection;
    end check_lost_update;
    --
    l_b  := utl_keyval_api.diff(in_ctx1 => utl_keyval_api.co_db_ctx, in_ctx2 => utl_keyval_api.co_apex_ctx, in_sub_ctx => utl_constants.co_b).count > 0;
    l_tl := l_b or utl_keyval_api.diff(in_ctx1 => utl_keyval_api.co_db_ctx, in_ctx2 => utl_keyval_api.co_apex_ctx, in_sub_ctx => utl_constants.co_tl).count > 0;
    --
    if l_b
    then
       l_id_old     := lr_&<name = "alias" lowercase = "true" >_db.&<name = "alias" lowercase = "true" >_id;
       l_&<name = "alias" lowercase = "true" >_id_old := lr_&<name = "alias" lowercase = "true" >_db.&<name = "alias" lowercase = "true" >_id;
    else
       l_id_old := lr_&<name = "alias" lowercase = "true" >_db.id;
    end if;
    --
    if l_b or
       l_tl
    then
       utl_dml_events_api.set_event(in_det_code => '&<name = "alias" lowercase = "true" >', in_operation => utl_dml_events_api.co_operation_update);
       cancel_&<name = "alias" lowercase = "true" >_internal(in_id => l_id_old, in_cancel_b => l_b);
       lr_&<name = "alias" lowercase = "true" > := ins_&<name = "alias" lowercase = "true" >_internal(in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >, in_&<name = "alias" lowercase = "true" >_id_old => l_&<name = "alias" lowercase = "true" >_id_old, in_ins_b => l_b, in_ins_tl => l_tl);
    end if;
    --
    lt_params.delete;
    append_par(io_params => lt_params, in_&<name = "alias" lowercase = "true" > => lr_&<name = "alias" lowercase = "true" >);
    logger.log(p_text => 'END', p_params => lt_params, p_scope => co_scope);
    --
    return lr_&<name = "alias" lowercase = "true" >;
  exception
    when sys_public_types.e_update_protection then
      logger.log(p_text => 'END', p_scope => co_scope);
      raise;
    when others then
      rollback to sp$&<name = "alias" lowercase = "true" >;
      logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope,  p_params => lt_params);
      raise;
  end upd_&<name = "alias" lowercase = "true" >;
