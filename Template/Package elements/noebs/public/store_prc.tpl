  /**
  * Stores the given &<name = "comment"> as UTL_KEYVAL_API context.
  *
  * @param in_ctx UTL_KEYVAL_API context.
  * @param in_&<name = "alias" lowercase = "true" > Values to store (&<name = "table" lowercase = "true" >_v record).
  */
  PROCEDURE store_&<name = "alias" lowercase = "true" >(in_ctx IN sys_public_types.str_type,
                      in_&<name = "alias" lowercase = "true" > IN &<name = "table" lowercase = "true" >_v%ROWTYPE) IS
    --
    -- co_scope Logger scope
    co_scope CONSTANT logger_logs.scope%TYPE := co_scope_prefix || 'store_&<name = "alias" lowercase = "true" >';
    --
    -- lt_params Logger parameters.
    lt_params logger.tab_param;
  BEGIN
    utl_keyval_api.init_ctx(in_ctx => in_ctx, in_sub_ctx => utl_constants.co_b);
    [#]
    utl_keyval_api.init_ctx(in_ctx => in_ctx, in_sub_ctx => utl_constants.co_tl);
  EXCEPTION
    WHEN OTHERS THEN
      logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
      RAISE;
  END store_&<name = "alias" lowercase = "true" >;
