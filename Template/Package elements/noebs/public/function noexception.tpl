/**
* &<name = "Purpose">
*
* @return &<Name = "Return Value"> 
*/
function &<name = "Function Name" lowercase = "true" >(&<name = "Parameters">) return &<Name = "Return Type"> is
begin
  /*
  TODO: owner="$OSUSER" category="Finish" priority="1 - High" created="$DATE"
  text="Stubbed: Add functionality"
  */
  [#]
  --
  return ;
end &<name = "Function Name" lowercase = "true" >;
