  /*
  * Updates (DML event method) the &<name = "comment"> supplied.
  *
  * in_&<name = "alias" lowercase = "true" > Record to handle.
  * in_dev DML event.
  *
  * Returns Updated record.
  */
  FUNCTION upd_&<name = "alias" lowercase = "true" >(in_&<name = "alias" lowercase = "true" > IN &<name = "table" lowercase = "true" >_v%ROWTYPE,
                   in_dev IN utl_dml_events%ROWTYPE) RETURN &<name = "table" lowercase = "true" >_v%ROWTYPE IS
    --
    -- co_scope Logger scope
    co_scope CONSTANT logger_logs.scope%TYPE := co_scope_prefix || 'upd_&<name = "alias" lowercase = "true" >';
    --
    --
    -- lt_params Logger parameters.
    -- lr_&<name = "alias" lowercase = "true" >    Record to handle
    -- lr_&<name = "alias" lowercase = "true" >_db Current values in the database.
    -- l_created DML event date.
    -- l_reg_id  DML event.
    lt_params logger.tab_param;
    lr_&<name = "alias" lowercase = "true" >    &<name = "table" lowercase = "true" >_v%ROWTYPE;
    lr_&<name = "alias" lowercase = "true" >_db &<name = "table" lowercase = "true" >_v%ROWTYPE;
    l_created utl_dml_events.created%TYPE;
    l_reg_id  utl_dml_events.id%TYPE;

  BEGIN
    &<name = "tapi" lowercase = "true" >.append_par(io_params => lt_params, in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >);
    logger.log(p_text => 'START', p_params => lt_params, p_scope => co_scope);
    --
    l_created := utl_db_context.created;
    l_reg_id  := utl_db_context.event_id;
    --
    <<check_lost_update>>
    BEGIN
      SELECT &<name = "alias" lowercase = "true" >.*
      INTO   lr_&<name = "alias" lowercase = "true" >_db
      FROM   &<name = "table" lowercase = "true" >_v &<name = "alias" lowercase = "true" >
      WHERE  &<name = "alias" lowercase = "true" >.id = in_&<name = "alias" lowercase = "true" >.id
      AND    &<name = "alias" lowercase = "true" >.row_version = in_&<name = "alias" lowercase = "true" >.row_version;
      --
      &<name = "tapi" lowercase = "true" >.store_&<name = "alias" lowercase = "true" >(in_ctx => utl_keyval_api.co_db_ctx, in_&<name = "alias" lowercase = "true" > => lr_&<name = "alias" lowercase = "true" >_db);
      &<name = "tapi" lowercase = "true" >.store_&<name = "alias" lowercase = "true" >(in_ctx => utl_keyval_api.co_apex_ctx, in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >);
    EXCEPTION
      WHEN no_data_found THEN
        RAISE sys_public_types.e_update_protection;
    END check_lost_update;
    --
    IF utl_keyval_api.diff(in_context1 => utl_keyval_api.co_db_ctx, in_context2 => utl_keyval_api.co_apex_ctx).count > 0
    THEN
      lr_&<name = "alias" lowercase = "true" > := &<name = "tapi" lowercase = "true" >.ins_&<name = "alias" lowercase = "true" >(in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >, in_&<name = "alias" lowercase = "true" >_id_old => lr_&<name = "alias" lowercase = "true" >_db.&<name = "alias" lowercase = "true" >_id);
      &<name = "tapi" lowercase = "true" >.cancel_&<name = "alias" lowercase = "true" >(in_&<name = "alias" lowercase = "true" >_id => lr_&<name = "alias" lowercase = "true" >_db.&<name = "alias" lowercase = "true" >_id, in_cancel_id => l_reg_id, in_canceled => l_created);
    END IF;
    --
    logger.log(p_text => 'END', p_scope => co_scope);
    --
    RETURN lr_&<name = "alias" lowercase = "true" >;
  EXCEPTION
    WHEN sys_public_types.e_update_protection THEN
      logger.log(p_text => 'END', p_scope => co_scope);
      RAISE;
    WHEN OTHERS THEN
      logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
      RAISE;
  END upd_&<name = "alias" lowercase = "true" >;
