   /**
   * &<name = "Purpose">
   */
   procedure &< name = "Procedure Name" lowercase = "true" > (&< name = "Parameters" >) is
     --
     -- co_scope Logger scope
     co_scope constant logger_logs.scope%type :=  co_scope_prefix || '&<Name = "Procedure Name" lowercase = "true" >';
     --
     -- lt_params Logger parameters.
     lt_params logger.tab_param;
   begin
     logger.log(p_text => sys_log_constants.co_start, p_params => lt_params, p_scope => co_scope);
     --
     --
     logger.log(p_text => sys_log_constants.co_end, p_scope => co_scope);
   exception
     when others then
          logger.log_error(p_text => sys_log_constants.co_exception, p_scope => co_scope,  p_params => lt_params);
          raise;                    
   end &< name = "Procedure Name" lowercase = "true" >;
