/**
* Inserts  a new &<name="comment"> in the &<name="comment"> tables (&<name = "table" lowercase = "true" > and &<name = "table" lowercase = "true" >_TL).
*
* @param in_&<name = "alias" lowercase = "true" >_rv     Record to handle.
* @param in_&<name = "alias" lowercase = "true" >_id_old The &<name = "comment"> which is updated using DML events. 
* @param in_ins_b      TRUE when the base record must be inserted.
* @param in_ins_tl     TRUE when the TL record must be inserted.
*
* @return Record created (current language).
*/
function ins_&<name = "alias" lowercase = "true" >_private(in_&<name = "alias" lowercase = "true" > in &<name = "table" lowercase = "true" >_v%rowtype, in_&<name = "alias" lowercase = "true" >_id_old in number default null, in_ins_b in boolean default true, in_ins_tl in boolean default true) return &<name = "table" lowercase = "true" >_v%rowtype is
   --
   -- co_scope Logger scope
   co_scope constant logger_logs.scope%type :=  co_scope_prefix || 'ins_&<name = "alias" lowercase = "true" >_private';
   --
   -- lt_params Logger parameters.
   -- lr_&<name = "alias" lowercase = "true" > Record to insert
   -- l_created DML event date.
   -- l_reg_id  DML event.
   -- l_lang    Current language
   lt_params logger.tab_param;
   lr_&<name = "alias" lowercase = "true" > &<name = "table" lowercase = "true" >_v%rowtype;
   l_created utl_dml_events.created%type;
   l_reg_id  utl_dml_events.id%type;
   l_lang    utl_languages.code%type;  
begin
   append_par(io_params => lt_params, in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >);
   logger.log(p_text => sys_log_constants.co_start, p_params => lt_params, p_scope => co_scope);
   --
   l_created := coalesce(in_<name = "alias">.created, sys_db_context.event_date);
   l_reg_id  := coalesce(in_<name = "alias">.reg_id, sys_db_context.event_id);
   l_lang    := utl_db_context.tl_lang;
   --
   lr_&<name = "alias" lowercase = "true" > := in_&<name = "alias" lowercase = "true" >;
   if in_ins_b
   then 
      --
      -- NoFormat Start
      insert into &<name = "table" lowercase = "true" >
         (id
         ,reg_id
         ,created)
      values
         (to_number(sys_guid(), sys_public_types.co_id_format_mask)
         ,l_reg_id
         ,l_created)
      returning 
      into ;
      -- NoFormat End
   end if;
   --
   if in_ins_tl
   then
      --
      insert into &<name = "table" lowercase = "true" >_tl
         (id
         ,lang
         ,reg_id
         ,created)
      values
         (to_number(sys_guid(), sys_public_types.co_id_format_mask)
        ,l_lang
        ,l_reg_id
        ,l_created)
      returning 
      into ;
      -- NoFormat End
   end if;
   --
   if in_ins_b
   then 
      translate_&<name = "alias" lowercase = "true" >(in_id => lr_&<name = "alias" lowercase = "true" >.id, in_&<name = "alias" lowercase = "true">_id => lr_&<name = "alias" lowercase = "true" >.&<name = "alias" lowercase = "true">_id, in_&<name = "alias" lowercase = "true" >_id_old => in_&<name = "alias" lowercase = "true" >_id_old);
   end if;
   --
   lt_params.delete;
   append_par(io_params => lt_params, in_&<name = "alias" lowercase = "true" > => lr_&<name = "alias" lowercase = "true" >);
   logger.log(p_text => sys_log_constants.co_end, p_params => lt_params, p_scope => co_scope);
   --
   return lr_&<name = "alias" lowercase = "true" >;
exception
   when others then
      logger.log_error(p_text => sys_log_constants.co_exception, p_scope => co_scope,  p_params => lt_params);
      raise;                    
end ins_&<name = "alias" lowercase = "true" >_private;
  