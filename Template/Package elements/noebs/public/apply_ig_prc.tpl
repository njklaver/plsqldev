  /**
  * APEX Interactive Grid apply procedure. 
  *
  * %param in_&<name = "alias" lowercase = "true">_rv &<name = "comment"> to handle.
  */
  PROCEDURE apply_&<name = "alias" lowercase = "true" >_ig(in_&<name = "alias" lowercase = "true" >_rv IN &<name = "table" lowercase = "true" >_v%rowtype) IS
    --
    -- co_unit Program unit name. Used in logging.
    co_unit CONSTANT sys_public_types.object_name_type := 'apply_&<name = "alias" lowercase = "true" >_ig';
    r_&<name = "alias" lowercase = "true" >_vl &<name = "table" lowercase = "true" >_vl%ROWTYPE;
  BEGIN
    utl_log.enter(in_module => co_package_name, in_unit => co_unit);
    &<name = "Table"  lowercase = "true" >_tapi.append_par(in_&<name = "alias" lowercase = "true" >_rv => in_&<name = "alias" lowercase = "true" >_rv);
    utl_log.set_par('APEX$ROW_STATUS', v('APEX$ROW_STATUS'));
    utl_log.close_entry;
    --
    CASE v('APEX$ROW_STATUS')
      WHEN 'C' THEN
        r_&<name = "alias" lowercase = "true" >_vl := &<name = "Table"  lowercase = "true" >_tapi.ins_&<name = "alias" lowercase = "true" >(in_&<name = "alias" lowercase = "true" >_rv => in_&<name = "alias" lowercase = "true" >_rv);
        apex_util.set_session_state(p_name => 'ID', p_value => r_&<name = "alias" lowercase = "true" >_vl.id, p_commit => FALSE);
      WHEN 'U' THEN
        r_&<name = "alias" lowercase = "true" >_vl := &<name = "Table"  lowercase = "true" >_tapi.upd_&<name = "alias" lowercase = "true" >(in_&<name = "alias" lowercase = "true" >_rv => in_&<name = "alias" lowercase = "true" >_rv);
      WHEN 'D' THEN
        &<name = "Table"  lowercase = "true" >_tapi.del_&<name = "alias" lowercase = "true" >(in_&<name = "alias" lowercase = "true" >_rv => in_&<name = "alias" lowercase = "true" >_rv);
    END CASE;
    --
    utl_log.leave(in_module => co_package_name, in_unit => co_unit);
    &<name = "Table"  lowercase = "true" >_tapi.append_par(in_&<name = "alias" lowercase = "true" >rec => r_&<name = "alias" lowercase = "true" >_vl);
    utl_log.close_entry;
    utl_log.flush;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_package_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END apply_&<name = "alias" lowercase = "true" >_ig;
