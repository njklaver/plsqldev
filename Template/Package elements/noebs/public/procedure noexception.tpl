/**
* &<Name = "Purpose">
*/
procedure &<name = "Procedure Name" lowercase = "true" >(&<name = "Parameters">) is
begin
  /*
  TODO: owner="$OSUSER" category="Finish" priority="1 - High" created="$DATE"
  text="Stubbed: Add functionality"
  */
  [#]
end &<name = "Procedure Name" lowercase = "true" >;
