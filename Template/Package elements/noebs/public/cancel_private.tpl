   /**
   * Cancels the given &<name = "comment">
   *
   * @param in_&<name = "alias" lowercase = "true" >_id     &<name = "comment"> to cancel.
   * @param in_cancel_b  TRUE when the base record must be canceled
   */
   procedure cancel_&<name = "alias" lowercase = "true" >_private(in_id    in number,
                    in_cancel_b in boolean default true) is
     --
     -- co_scope Logger scope
     co_scope constant logger_logs.scope%type := co_scope_prefix || 'cancel_private';
     --
     -- lt_params   Logger parameters.
     -- l_cancel_id Cancel DML event 
     -- l_canceled  Cancel date
     lt_params logger.tab_param;
     l_cancel_id sys_dml_events.id%TYPE;
     l_canceled  sys_dml_events.created%TYPE;
  begin
     logger.append_param(p_params => lt_params, p_name => 'in_id', p_val => in_id);
     logger.append_param(p_params => lt_params, p_name => 'in_cancel_b', p_val => in_cancel_b);
     logger.log(p_text => sys_log_constants.co_start, p_params => lt_params, p_scope => co_scope);
      --
      l_canceled  := localtimestamp;
      l_cancel_id := sys_db_context.event_id;
      --
      if in_cancel_b
      then
         update &<name = "table" lowercase = "true" > &<name = "alias" lowercase = "true" >
         set    &<name = "alias" lowercase = "true" >.cancel_id = l_cancel_id,
                &<name = "alias" lowercase = "true" >.canceled  = l_canceled
         where  &<name = "alias" lowercase = "true" >.id = in_id;
         --
         update &<name = "table" lowercase = "true" >_tl &<name = "alias" lowercase = "true" >tl
         set    &<name = "alias" lowercase = "true" >tl.cancel_id = l_cancel_id,
                &<name = "alias" lowercase = "true" >tl.canceled  = l_canceled
         where  &<name = "alias" lowercase = "true" >tl.&<name = "alias" lowercase = "true" >_id = in_id;
      else
         update &<name = "table" lowercase = "true" >_tl &<name = "alias" lowercase = "true" >tl
         set    &<name = "alias" lowercase = "true" >tl.cancel_id = l_cancel_id,
                &<name = "alias" lowercase = "true" >tl.canceled  = l_canceled
         where  &<name = "alias" lowercase = "true" >tl.id = in_id;
      end if;
      --
      logger.log(p_text => sys_log_constants.co_start, p_scope => co_scope);
   exception
      when others then
         logger.log_error(p_text => sys_log_constants.co_exception, p_scope => co_scope, p_params => lt_params);
         raise;
   end cancel_&<name = "alias" lowercase = "true" >private;
