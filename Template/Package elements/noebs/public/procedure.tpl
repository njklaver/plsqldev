/**
* &<Name = "Purpose">
*
*/
procedure &<NAME = "Procedure Name" lowercase = "true" >(&<NAME = "Parameters">) is
  --
  -- co_scope Logger scope
  co_scope constant logger_logs.scope%type :=  co_scope_prefix || '&<Name = "Procedure Name" lowercase = "true" >';
  --
  -- lt_params Logger parameters.
  lt_params logger.tab_param;
begin
  /*
  TODO: owner="$OSUSER" category="Finish" priority="1 - High" created="$DATE"
  text="Stubbed: Add functionality"
  */
  [#]
exception
  when others then
       logger.log_error(p_text => sys_log_constants.co_exception, p_scope => co_scope,  p_params => lt_params);
       raise;                    
end &<NAME = "Procedure Name" lowercase = "true" >;
