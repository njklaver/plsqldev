  /**
  * Logs the record supplied.
  *
  * @param io_params Log params. 
  * @param in_&<name = "alias" lowercase = "true" >   Values to log (&<name = "table" lowercase = "true" >_v record).
  */
  PROCEDURE append_par(io_params IN OUT NOCOPY logger.tab_param, in_&<name = "alias" lowercase = "true" >  IN &<name = "table" lowercase = "true" >_v%ROWTYPE) IS
  BEGIN
    [#]
  END append_par;
