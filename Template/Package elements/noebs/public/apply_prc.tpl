   /**
   * APEX apply &<name="comment"> procedure. 
   *
   * @param in_&<name = "alias" lowercase = "true" > &<name = "comment"> to handle.
   * @param in_apex$row_status DML operation 
   * @param in_page_id         APEX page
   * @param out_id             PK &<name="comment">
   */
   procedure apply(in_&<name = "alias" lowercase = "true" > in &<name = "table" lowercase = "true" >_v%rowtype
                  ,in_apex$row_status in         varchar2
                  ,in_page_id         in         number
                  ,out_id             out nocopy number) is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'apply_&<name = "alias" lowercase = "true" >';
      --
      -- lt_params_in Logger parameters.
      lt_params  logger.tab_param;
      --
      -- lr_&<name = "alias" lowercase = "true" > Record to handle
      -- lr_dev DML event
      lr_&<name = "alias" lowercase = "true" > &<name = "table" lowercase = "true" >_v%rowtype;
   begin
      logger.append_param(p_params => lt_params, p_name => 'in_apex$row_status', p_val => in_apex$row_status);
      logger.append_param(p_params => lt_params, p_name => 'in_page_id', p_val => in_page_id);
      &<name = "tapi" lowercase = "true" >.append_par(io_params => lt_params, in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >);
      logger.append_param(p_params => lt_params_in, p_name => 'in_apex$row_status', p_val => in_apex$row_status);
      logger.log(p_text => 'start', p_params => lt_params, p_scope => co_scope);
      --
      utl_dml_events_api.set_parent(in_det_code => utl_dml_events_api.co_apex, in_process_name => co_scope, in_page_id => in_page_id);
      --
      case in_apex$row_status
         when 'C' then
            lr_&<name = "alias" lowercase = "true" > := &<name = "tapi" lowercase = "true" >.ins(in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >);
            out_id := lr_&<name = "alias" lowercase = "true" >.id;
         when 'U' then
            lr_&<name = "alias" lowercase = "true" > := &<name = "tapi" lowercase = "true" >.upd(in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >);
            out_id := lr_&<name = "alias" lowercase = "true" >.id;
         when 'D' then
            &<name = "tapi" lowercase = "true" >.cancel(in_id => in_&<name = "alias" lowercase = "true" >.&<name = "alias" lowercase = "true" >_id);
      end case;
      --
      if utl_error.has_errors
      then
         raise sys_public_types.e_validation_failed;
      end if;
      lt_params.delete;
      &<name = "tapi" lowercase = "true" >.append_par(io_params => lt_params, in_&<name = "alias" lowercase = "true" > => lr_&<name = "alias" lowercase = "true" >);
      logger.log(p_text => 'end', p_params => lt_params, p_scope => co_scope);
   exception
      when sys_exceptions.e_update_protection then
         logger.log(p_text => sys_log_constants.co_update_protection, p_scope => co_scope);
         sys_message_api.flush_apex;
      when sys_exceptions.e_validation_failed then
         logger.log(p_text => sys_log_constants.co_validation_failed, p_scope => co_scope);
         sys_message_api.flush_apex;
      when others then
         logger.log_error(p_text => sys_log_constants.co_exception, p_scope => co_scope, p_params => lt_params);
         raise;
   end apply_&<name = "alias" lowercase = "true" >;
