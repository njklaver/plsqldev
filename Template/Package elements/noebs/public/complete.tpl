  /*
  * Completes the  &<name = "comment"> record given. 
  *
  * io_&<name = "alias" lowercase = "true">_rv &<name = "comment"> to handle.
  */
  PROCEDURE complete(io_&<name = "alias" lowercase = "true">_rv   IN OUT NOCOPY &<name = "Table" lowercase = true>_v%ROWTYPE) IS
    --
    -- co_unit Program unit name. Used in logging.
    co_unit CONSTANT sys_public_types.object_name_type := 'apply_&<name = "alias" lowercase = "true">_form';
    --
  BEGIN
    utl_log.enter(in_module => co_package_name, in_unit => co_unit);
    &<name = "Table" lowercase = true>_tapi.append_par(in_&<name = "alias" lowercase = "true">_rv => io_&<name = "alias" lowercase = "true">_rv);
    utl_log.close_entry;
    --
    [#]
    --
    utl_log.leave(in_module => co_package_name, in_unit => co_unit);
    &<name = "Table" lowercase = true>_tapi.append_par(in_&<name = "alias" lowercase = "true">_rv => io_&<name = "alias" lowercase = "true">_rv);
    utl_log.close_entry;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_package_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END complete;
