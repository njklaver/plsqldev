  /**
  * Creates the missing &<name = "comment"> records for all mapped languages for the current application. 
  *
  * @param in_id         TL record to use as template.
  * @param in_&<name = "alias" lowercase = "true" >_id     The &<name = "comment"> to handle 
  * @param in_&<name = "alias" lowercase = "true" >_id_old The &<name = "comment"> which is updated using DML events. 
  */
  procedure translate(in_id in number, in_ &< name = "alias" lowercase = "true" > _id in number, in_ &< name = "alias" lowercase = "true" > _id_old in number) is
     --
     -- co_scope Logger scope
     co_scope constant logger_logs.scope%type := co_scope_prefix || 'translate';
     --
     -- lt_params   Logger parameters.
     -- lt_lang     Languages already in the database.
     -- lt_map_lang Mapped languages.
     -- l_created   DML event date.
     -- l_reg_id    DML event.
     lt_params   logger.tab_param;
     lt_lang     apex_t_varchar2;
     lt_map_lang apex_t_varchar2;
     l_created   sys_dml_events.created%type;
     l_reg_id    sys_dml_events.id%type;
  begin
     logger.append_param(p_params => lt_params, p_name => 'in_id', p_val => in_id);
     logger.append_param(p_params => lt_params,p_name   => 'in_&<name = "alias" lowercase = "true">_id',p_val    => in_ &< name = "alias" lowercase = "true" > _id);
     logger.log(p_text => sys_log_constants.co_start, p_params => lt_params, p_scope => co_scope);
     --
     l_created := sys_db_context.event_date;
     l_reg_id  := sys_db_context.event_id;
     --
     if in_ &< name = "alias" lowercase = "true" > _id_old is not null
     then
        insert into &< name = "table" lowercase = "true" > _tl
           (id
           ,&< name = "alias" lowercase = "true" > _id
            ,meaning
            ,description
            ,lang
            ,created
            ,reg_id)
           select to_number(sys_guid(), sys_public_types.co_id_format_mask)
                 ,in_ &< name = "alias" lowercase = "true" > _id
                 ,&< name = "alias" lowercase = "true" > tl.meaning
                 ,&< name = "alias" lowercase = "true" > tl.description
                 ,&< name = "alias" lowercase = "true" > tl.lang
                 ,l_created
                 ,l_reg_id
             from &< name = "table" lowercase = "true" > _tl &< name = "alias" lowercase = "true" >_tl
            where &< name = "alias" lowercase = "true" > tl. &< name = "alias" lowercase = "true" >_id = in_ &< name = "alias" lowercase = "true" >_id
              and &< name = "alias" lowercase = "true" > tl.id <> in_id;
     end if;
     --
     select distinct lang
       bulk collect
       into lt_lang
       from &< name = "table" lowercase = "true" > _tl &< name = "alias" lowercase = "true" >_tl
      where &< name = "alias" lowercase = "true" > tl. &< name = "alias" lowercase = "true" >_id = in_ &< name = "alias" lowercase = "true" >_id;
     --
     lt_map_lang := sys_language_api.map_lang multiset except lt_lang;
     insert into &< name = "table" lowercase = "true" > _tl
        (id
        ,&< name = "alias" lowercase = "true" > _id
         ,meaning
         ,description
         ,lang
         ,created
         ,reg_id)
        select to_number(sys_guid(), sys_public_types.co_id_format_mask)
              ,&< name = "alias" lowercase = "true" > _tl. &< name = "alias" lowercase = "true" >_id
               ,&< name = "alias" lowercase = "true" > _tl.meaning
               ,&< name = "alias" lowercase = "true" > _tl.description
               ,column_value
               ,l_created
               ,l_reg_id
          from &< name = "table" lowercase = "true" > _tl &< name = "alias" lowercase = "true" >_tl
         cross join (select column_value
                       from table(lt_map_lang))
         where &< name = "alias" lowercase = "true" > _tl.id = in_id;
     --
     logger.log(p_text => sys_log_constants.co_end, p_scope => co_scope);
  exception
     when others then
        logger.log_error(p_text => sys_log_constants.co_exception, p_scope => co_scope, p_params => lt_params);
        raise;
  end translate;
