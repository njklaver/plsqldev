/**
* &<Name = "Constant Description">
*/
&<Name = "Constant Name" lowercase = "Yes" default = "g"> constant &<Name = "Constant Type" lowercase = "Yes" required = "Yes"> := &<Name = "Constant Value" required = "Yes">;
