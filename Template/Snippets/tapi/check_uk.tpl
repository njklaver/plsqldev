<<&<Name="Constraint" lowercase="Yes">>>
begin
  if pb_uk
  then
    [#]
  end if;
  --
  select '&<Name="Constraint">' into lc_constraint from
  --
  utl_msg_util.constraint_error(lc_constraint, lt_uk_val, sys_public_types.gn_error);
  lx_return_status := sys_public_types.gc_ret_sts_error;
exception
  when no_data_found then
    null;
end &<Name="Constraint">;
