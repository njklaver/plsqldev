--
-- t_&<Name = "Alias" lowercase = "True" required = "True">_type Resultset for C_&<Name = "Alias" uppercase = "True" required = "True">.
TYPE t_&<Name = "Alias" lowercase = "True" required = "True">_type is TABLE OF c_&<Name = "Alias" uppercase = "True" required = "True">%rowtype index by sys_public_types.idx_type;
--
-- l_cnt Number of records in T_&<Name = "Alias" uppercase = "True" required = "True">.
-- t_&<Name = "Alias" lowercase = "True" required = "True"> Resultset for C_&<Name = "Alias" uppercase = "True" required = "True">.
l_cnt pls_integer;
t_&<Name = "Alias" lowercase = "True" required = "True"> t_&<Name = "Alias" lowercase = "True" required = "True">_type;
