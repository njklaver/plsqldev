/**
* &<Name = "Table Description">
*/
type &<Name = "Table Name" default = "t_#_tab" lowercase = "Yes"> is table of &<name="Element datatype" lowercase = "Yes" required = "Yes"> index by sys_public_types.t_idx;
/**
* &<Name = "Table Description">
*/
g&<Name = "Table Name"> &<Name = "Table Name">;
