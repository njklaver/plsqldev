/**
* &<Name = "SubType Description">
*/
subtype &<Name = "SubType Name" lowercase = "Yes"  default = "t_"> is &<Name = "Data Type"  lowercase = "Yes" required = "Yes">;
