/**
* Parameter &<name="Name"> (logging)
*/
co_&<name="Name"> constant sys_public_types.object_name_type := '&<name="Name">';
