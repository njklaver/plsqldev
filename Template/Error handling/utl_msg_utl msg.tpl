/**
* Sets a UTL_&<name="Error" uppercase="yes" required="yes"> error.
*
* @return UTL_&<name="Error" uppercase="yes" required="yes"> error message 
*/
function &<name="Error" required="yes">(&<name="Tokens">) RETURN sys_public_types.text_type is
   --
   -- co_scope         Logger scope
   -- co_&<name="Error" lowercase="yes" required="yes"> Message code.
   co_scope          CONSTANT logger_logs.scope%TYPE := co_scope_prefix || '&<name="Error" lowercase="yes" required="yes">';
   co_&<name="Error"> CONSTANT sys_public_types.code_type :=  'UTL_&<name="Error">';
   --
   -- lt_params Logger parameters.
   lt_params logger.tab_param;
begin
   utl_msg_api.set_name(in_msg_templ_code => co_&<name="Error">);
   --
   RETURN utl_msg_api.get();

exception
   when others then
      logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
      RAISE;
 end &<name="Error">;