  /**
  * Sets a &<name="Error" required=true> error.
  *
  * %param pn_msg_severity Message severity:
  *                        {*} 0 Informational.
  *                        {*} 1 Warning.
  *                        {*} 2 Error.
  *                        {*} 3 Unexpected error.
  *                        {*} 4 Internal error.
  */
  procedure &<name="Error" lowercase="yes">(&<name="Tokens">, pn_msg_severity in sys_public_types.t_msg_severity) is
      --
      -- x_unit Program unit name.
      x_unit constant sys_public_types.t_object_name := '&<name="Error" lowercase="yes">';
  begin
    set_message('&<name="Error" uppercase="yes">', pn_msg_severity);
    --
    push();
    exception
      when others then
        utl_log.unexpected(pc_module => package_name, pc_unit => x_unit, pc_log_msg => sqlerrm);
   end &<name="Error" lowercase="yes">;