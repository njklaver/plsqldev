/**
* &<Name = "Purpose">
*
* %param x_return_status Return status (<code>GC_RET_STS_SUCCESS, GC_RET_STS_ERROR or GC_RET_STS_UNEXP_ERROR</code>).
*
* %return &<Name = "Return Value"> 
*/
&<Name = "Overriding?" checkbox="overriding ,">member function &<Name = "Function Name" lowercase = "True">(SELF &<Name ="Setter?" checkbox="IN OUT, IN"> &<Name ="Type" lowercase = "True">, &<name = "Parameters" Suffix = ","> x_return_status in out nocopy varchar2) return &<Name = "Return Type"> is
    --
    -- x_type_name    Type name.
    -- x_unit         Program unit name.
    x_type_name CONSTANT sys_public_types.t_object_name := '&<Name ="Type" lowercase = "True">';
    x_unit      CONSTANT sys_public_types.t_object_name := '&<Name ="Function Name" lowercase = "True">';
  --
  -- lx_return_status Local value for <code>X_RETURN_STATUS</code>.
  lx_return_status sys_public_types.t_return_status;
begin
  lx_return_status := x_return_status;
  IF lx_return_status = sys_public_types.gc_ret_sts_unexp_error
  THEN
    RETURN;
  END IF;
  --
  utl_log.open_begin(x_type_name, x_unit);
  utl_log.close_entry;
  --
  /*
  TODO: owner="$OSUSER" category="Finish" priority="1 - High" created="$DATE"
  text="Stubbed: Add functionality"
  */
  [#]
  --
  utl_log.open_end;
  utl_log.close_entry;
  --
  x_return_status := lx_return_status;
  return;
exception
  when sys_public_types.e_unexpected then
    utl_exceptions.raise_error(sys_public_types.gn_unexpected, x_type_name, x_unit);
    x_return_status := sys_public_types.gc_ret_sts_unexp_error;
    return null;
  when sys_public_types.e_error then
    utl_exceptions.raise_error(sys_public_types.gn_error, x_type_name, x_unit);
    x_return_status := sys_public_types.gc_ret_sts_error;
    return null;
  when sys_public_types.e_warning then
    utl_exceptions.raise_error(sys_public_types.gn_warning, x_type_name, x_unit);
    x_return_status := sys_public_types.gc_ret_sts_warning;
    return null;
  when sys_public_types.e_fatal then
    raise;
  when others then
    if sqlcode = -20030
    then
      raise sys_public_types.e_fatal;
    end if;
    utl_msg_util.oracle_error(sqlerrm, x_unit, x_type_name);
    utl_exceptions.raise_error(sys_public_types.gn_unexpected, x_type_name, x_unit);
    x_return_status := sys_public_types.gc_ret_sts_unexp_error;
    return null;
end &<name = "Function Name" lowercase = "true" >;
