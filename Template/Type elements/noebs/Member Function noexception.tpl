/**
* &<Name = "Purpose">
*
* %return &<Name = "Return Value"> 
*/
&<Name = "Overriding?" checkbox="overriding ,">member function &<Name = "Function Name" lowercase = "True">(SELF &<Name ="Setter?" checkbox="IN OUT, IN"> &<Name ="Type" lowercase = "True">, &<name = "Parameters">) return &<Name = "Return Type"> is
    --
    -- x_type_name    Type name.
    -- x_unit         Program unit name.
    x_type_name CONSTANT sys_public_types.t_object_name := '&<Name ="Type" lowercase = "True">';
    x_unit      CONSTANT sys_public_types.t_object_name := '&<Name ="Function Name" lowercase = "True">';
begin
  /*
  TODO: owner="$OSUSER" category="Finish" priority="1 - High" created="$DATE"
  text="Stubbed: Add functionality"
  */
  [#]
  --
  return;
end &<name = "Function Name" lowercase = "true" >;
