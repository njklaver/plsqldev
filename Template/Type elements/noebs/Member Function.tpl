/**
* &<Name = "Purpose">
*
* %return &<Name = "Return Value"> 
*/
&<Name = "Overriding?" checkbox="overriding ,">member function &<Name = "Function Name" lowercase = "True">(SELF &<Name ="Setter?" checkbox="IN OUT, IN"> &<Name ="Type" lowercase = "True">, &<name = "Parameters">) return &<Name = "Return Type"> is
  --
  -- co_type_name Type name. Used in logging.
  -- co_unit      Program unit name. Used in logging.
  co_type_name CONSTANT sys_public_types.object_name_type := '&<Name ="Type" lowercase = "True">';
  co_unit      CONSTANT sys_public_types.object_name_type := '&<Name ="Function Name" lowercase = "True">';
begin
  /*
  TODO: owner="$OSUSER" category="Finish" priority="1 - High" created="$DATE"
  text="Stubbed: Add functionality"
  */
  [#]
  --
  return; 
exception
  when others then
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
end &<name = "Function Name" lowercase = "true" >;
