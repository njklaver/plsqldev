create or replace type &<Name = "Table Type Name" Required = "Yes"> force
  /**
  * &<Name = "Purpose">
  */
is table OF &<Name = "Table Element Type" Required = "Yes">;
