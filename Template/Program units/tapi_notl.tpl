create or replace package &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_tapi is
   /**
   * Table API for &<Name = "Comment">
   */


   /**
   * Returns the package release
   * 
   * @return the package release
   */
   function release return sys_public_types.code_type;

   /**
   * Logs the record supplied.
   *
   * @param io_params Log params. 
   * @param in_&<name = "Alias" lowercase = "true" >   Values to log (&<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes"> record).
   */
   procedure append_par(io_params in out nocopy logger.tab_param, in_&<name = "Alias" lowercase = "true" >  in &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype);

   /**
   * Stores the given &<name = "Comment"> as SYS_KEYVAL_API context.
   *
   * @param in_ctx SYS_KEYVAL_API context.
   * @param in_&<name = "Alias" lowercase = "true" > Values to store (&<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes"> record).
   */
   procedure store(in_ctx in sys_public_types.str_type,
                   in_&<name = "Alias" lowercase = "true" > in &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype);

   /**
   * Inserts  a new &<name="Comment"> in the &<name="Comment"> tables (&<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes"> and&<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_TL).
   *
   * @param in_&<name = "Alias" lowercase = "true" > Record to handle.
   *
   * @return Record created (current language).
   */
   function ins(in_&<name = "Alias" lowercase = "true" > in &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype) return &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype;

   /**
   * Updates (DML event method) the  &<name = "Comment"> supplied.
   *
   * @param in_&<name = "Alias" lowercase = "true" > Record to handle.
   *
   * @return Record updated (current language).
   */
   function upd(in_&<name = "Alias" lowercase = "true" > in &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype) return &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype;

   /**
   * Cancels the given &<name = "Comment" >
   *
   * @param in_id &<name = "Comment"> to cancel.
   */
   procedure cancel(in_id in integer);
end  &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_tapi;
/
create or replace package body &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_tapi is
   /*
   * Table API for &<Name = "Comment">
   *
   * History
   * Date       Who         Release Jira           Comments
   * ---------- ----------- ------- -------------- --------------------------------------------------------------------------------
   * &<name="Date"> Nico Klaver &<Name = Release> &<Name = Jira> First creation       
   */
   /**
   * Release
   */
   co_release constant sys_public_types.code_type := '1.0.0';

   /*
   * co_scope_prefix Logger scope prefix. 
   */
   co_scope_prefix constant sys_public_types.object_name_type := lower($$plsql_unit) || '.';

   /**
   * Parameter logging
   */

   /**
   * in_&<name="Alias"> record logging
   */

   /*
   * DML event type for &<Name = comment>.
   */
   co_det_code constant sys_public_types.code_type := sys_dml_event_types_api.event_type(in_ent_name => '&<name = "Base Name">');

   /**
   * Cancels the given &<name = "Comment">
   *
   * @param in_id         &<name = "Comment"> to cancel (PK base record).
   * @param in_cancel_b   TRUE when the base record must be canceled
   * @param in_cancel_att TRUE when the attributes record must be canceled
   */
   procedure cancel_private(in_id       in number
                           ,in_cancel_b   in boolean default true
                           ,in_cancel_att in boolean default true) is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'cancel_private';
      --
      -- lt_params   Logger parameters.
      -- l_cancel_id Cancel DML event 
      -- l_canceled  Cancel date
      lt_params logger.tab_param;
      l_cancel_id sys_dml_events.id%TYPE;
      l_canceled  sys_dml_events.created%TYPE;
   begin
      logger.append_param(p_params => lt_params, p_name => sys_log_constants.co_in_id, p_val => in_id);
      logger.append_param(p_params => lt_params, p_name => sys_log_constants.co_in_cancel_b, p_val => in_cancel_b);
      logger.append_param(p_params => lt_params, p_name => sys_log_constants.co_in_cancel_att, p_val => in_cancel_att);
      logger.log(p_text => sys_log_constants.co_start, p_params => lt_params, p_scope => co_scope);
      --
      l_canceled  := sys_db_context.event_date;
      l_cancel_id := sys_db_context.event_id;
      --
      if in_cancel_b
      then
         update &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes"> &<name = "Alias" lowercase = "true" >
         set    &<name = "Alias" lowercase = "true" >.cancel_id = l_cancel_id,
                &<name = "Alias" lowercase = "true" >.canceled  = l_canceled
         where  &<name = "Alias" lowercase = "true" >.id = in_id;
         --
         update &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_att &<name = "Alias" lowercase = "true" >att
         set    &<name = "Alias" lowercase = "true" >att.cancel_id = l_cancel_id,
                &<name = "Alias" lowercase = "true" >att.canceled  = l_canceled
         where  &<name = "Alias" lowercase = "true" >att.&<name = "Alias" lowercase = "true" >_id = in_id
           and  &<name = "Alias" lowercase = "true" >att.cancel_id is null;
      else
         if in_cancel_att
         then
            update &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_att &<name = "Alias" lowercase = "true" >att
            set    &<name = "Alias" lowercase = "true" >att.cancel_id = l_cancel_id,
                   &<name = "Alias" lowercase = "true" >att.canceled  = l_canceled
            where  &<name = "Alias" lowercase = "true" >att.&<name = "Alias" lowercase = "true" >_id = in_id
              and  &<name = "Alias" lowercase = "true" >att.cancel_id is null;
         end if;
      end if;
      --
      logger.log(p_text => sys_log_constants.co_start, p_scope => co_scope);
   exception
      when others then
         logger.log_error(p_text => sys_log_constants.co_exception, p_scope => co_scope, p_params => lt_params);
         raise;
   end cancel_private;

   /**
   * Inserts  a new &<name="Comment"> in the &<name="Comment"> tables (&<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes"> and&<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_TL).
   *
   * @param in_&<name = "Alias" lowercase = "true" >Record to handle.
   * @param in_ins_b   TRUE when the base record must be inserted.
   * @param in_ins_att TRUE when the ATT record must be inserted.
   *
   * @return Record created (current language).
   */
   function ins_private(in_&<name = "Alias" lowercase = "true" > in &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype
                       ,in_ins_b   in boolean default true
                       ,in_ins_att in boolean default true) return &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type :=  co_scope_prefix || 'ins_private';
      --
      -- lt_params Logger parameters.
      -- lr_&<name = "Alias" lowercase = "true" > Record to insert
      -- l_created DML event date.
      -- l_reg_id  DML event.
      lt_params logger.tab_param;
      lr_&<name = "Alias" lowercase = "true" > &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype;
      l_created sys_dml_events.created%type;
      l_reg_id  sys_dml_events.id%type;
   begin
      append_par(io_params => lt_params, in_&<name = "Alias" lowercase = "true" > => in_&<name = "Alias" lowercase = "true" >);
      logger.append_param(p_params => lt_params, p_name => sys_log_constants.co_in_ins_b, p_val => in_ins_b);
      logger.append_param(p_params => lt_params, p_name => sys_log_constants.co_in_ins_att, p_val => in_ins_att);
      logger.log(p_text => sys_log_constants.co_start, p_params => lt_params, p_scope => co_scope);
      --
      l_created := coalesce(in_&<name = "Alias">.created, sys_db_context.event_date);
      l_reg_id  := coalesce(in_&<name = "Alias">.reg_id, sys_db_context.event_id);
      --
      lr_&<name = "Alias" lowercase = "true" > := in_&<name = "Alias" lowercase = "true" >;
      if in_ins_b
      then 
         --
         -- NoFormat Start
         insert into &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">
            (id
            ,reg_id
            ,created)
         values
            (to_number(sys_guid(), sys_public_types.co_id_format_mask)
            ,l_reg_id
            ,l_created)
         returning id
                  ,reg_id
                  ,created
              into lr_&<name = "Alias" lowercase = "true" >.&<name = "Alias" lowercase = "true" >_id
                  ,lr_&<name = "Alias" lowercase = "true" >.reg_id
                  ,lr_&<name = "Alias" lowercase = "true" >.created;
         -- NoFormat End
      end if;
      --
      if in_ins_att
      then
         --
         -- NoFormat Start
         insert into &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_att
            (id
            ,&<name = "Alias" lowercase = "true" >_id
            ,reg_id
            ,created)
         values
            (to_number(sys_guid(), sys_public_types.co_id_format_mask)
            ,lr_&<name = "Alias" lowercase = "true" >.&<name = "Alias" lowercase = "true" >_id
            ,l_reg_id
            ,l_created)
         returning id
                  ,reg_id
                  ,created
              into lr_&<name = "Alias" lowercase = "true" >.att_id
                  ,lr_&<name = "Alias" lowercase = "true" >.att_reg_id
                  ,lr_&<name = "Alias" lowercase = "true" >.att_created;
         -- NoFormat End
      end if;
      --
      lt_params.delete;
      append_par(io_params => lt_params, in_&<name = "Alias" lowercase = "true" > => lr_&<name = "Alias" lowercase = "true" >);
      logger.log(p_text => sys_log_constants.co_end, p_params => lt_params, p_scope => co_scope);
      --
      return lr_&<name = "Alias" lowercase = "true" >;
   exception
      when others then
         logger.log_error(p_text => sys_log_constants.co_exception, p_scope => co_scope,  p_params => lt_params);
         raise;                    
   end ins_private;

   /**
   * Returns the package release
   * 
   * @return the package release
   */
   function release return sys_public_types.code_type is
   begin
      return co_release;
   end release;   

   /**
   * Logs the record supplied.
   *
   * @param io_params Log params. 
   * @param in_&<name = "Alias" lowercase = "true" >   Values to log (&<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes"> record).
   */
   procedure append_par(io_params in out nocopy logger.tab_param, in_&<name = "Alias" lowercase = "true" >  in &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype) is
   begin
   end append_par;


   /**
   * Stores the given &<name = "Comment"> as SYS_KEYVAL_API context.
   *
   * @param in_ctx SYS_KEYVAL_API context.
   * @param in_&<name = "Alias" lowercase = "true" > Values to store (&<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes"> record).
   */
   procedure store(in_ctx in sys_public_types.str_type,
                   in_&<name = "Alias" lowercase = "true" > in &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype) is
    --
    -- co_scope Logger scope
    co_scope constant logger_logs.scope%type := co_scope_prefix || 'store';
    --
    -- lt_params Logger parameters.
    lt_params logger.tab_param;
  begin
    sys_keyval_api.init_ctx(in_ctx => in_ctx, in_sub_ctx => sys_keyval_api.co_att_ctx);
  exception
    when others then
      logger.log_error(p_text => sys_log_constants.co_exception, p_scope => co_scope, p_params => lt_params);
      raise;
  end store;

   /**
   * Inserts  a new &<name="Comment"> in the &<name="Comment"> tables (&<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes"> and&<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_TL).
   *
   * @param in_&<name = "Alias" lowercase = "true" > Record to handle.
   *
   * @return Record created (current language).
   */
   function ins(in_&<name = "Alias" lowercase = "true" > in &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype) return &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type :=  co_scope_prefix || 'ins';
      --
      -- lt_params Logger parameters.
      -- lr_&<name = "Alias" lowercase = "true" > Record to insert
      lt_params logger.tab_param;
      lr_&<name = "Alias" lowercase = "true" > &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype;
   begin
      append_par(io_params => lt_params, in_&<name = "Alias" lowercase = "true" > => in_&<name = "Alias" lowercase = "true" >);
      logger.log(p_text => sys_log_constants.co_start, p_params => lt_params, p_scope => co_scope);
      --
      sys_dml_events_api.set_event(in_det_code => co_det_code, in_operation => sys_dml_events_api.co_operation_create);    
      --
      lr_&<name = "Alias" lowercase = "true" > := ins_private(in_&<name = "Alias" lowercase = "true" > => in_&<name = "Alias" lowercase = "true" >);
      --
      lt_params.delete;
      append_par(io_params => lt_params, in_&<name = "Alias" lowercase = "true" > => lr_&<name = "Alias" lowercase = "true" >);
      logger.log(p_text => sys_log_constants.co_end, p_params => lt_params, p_scope => co_scope);
      --
      return lr_&<name = "Alias" lowercase = "true" >;
   exception
      when others then
         logger.log_error(p_text => sys_log_constants.co_exception, p_scope => co_scope,  p_params => lt_params);
         raise;                    
   end ins;

   /**
   * Updates (DML event method) the  &<name = "Comment"> supplied.
   *
   * @param in_&<name = "Alias" lowercase = "true" > Record to handle.
   *
   * @return Record updated (current language).
   */
   function upd(in_&<name = "Alias" lowercase = "true" > in &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype) return &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type :=  co_scope_prefix || 'upd';
      --
      --
      -- lt_params    Logger parameters.
      -- lr_&<name = "Alias" lowercase = "true" >       Record to handle
      -- lr_&<name = "Alias" lowercase = "true" >_db    Current values in the database.
      -- l_b          TRUE when the base record must be inserted.
      -- l_id_old     The &<Name = "Comment"> which is updated using DML events (base record).
      lt_params logger.tab_param;
      lr_&<name = "Alias" lowercase = "true" >      &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype;
      lr_&<name = "Alias" lowercase = "true" >_db   &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype;
      l_att       boolean;
      l_&<name = "Alias" lowercase = "true" >_id_old    &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v.&<name = "Alias" lowercase = "true" >_id%type;
   begin
      append_par(io_params => lt_params, in_&<name = "Alias" lowercase = "true" > => in_&<name = "Alias" lowercase = "true" >);
      logger.log(p_text => sys_log_constants.co_start, p_params => lt_params, p_scope => co_scope);
      --
      <<check_lost_update>>
      begin
       select &<name = "Alias" lowercase = "true" >.*
       into   lr_&<name = "Alias" lowercase = "true" >_db
       from   &<Name = "Prefix" lowercase = "true" required = "Yes">_g_&<Name = "Base Name" lowercase = "true" required = "Yes"> &<name = "Alias" lowercase = "true" >
       where  &<name = "Alias" lowercase = "true" >.&<name = "Alias" lowercase = "true" >_id = in_&<name = "Alias" lowercase = "true" >.&<name = "Alias" lowercase = "true" >_id;
       --
       store(in_ctx => sys_keyval_api.co_db_ctx, in_&<name = "Alias" lowercase = "true" > => lr_&<name = "Alias" lowercase = "true" >_db);
       store(in_ctx => sys_keyval_api.co_dml_ctx, in_&<name = "Alias" lowercase = "true" > => in_&<name = "Alias" lowercase = "true" >);
      exception
       when no_data_found then
          raise sys_exceptions.e_update_protection;
      end check_lost_update;
      --
      l_att  := sys_keyval_api.diff(in_ctx1 => sys_keyval_api.co_db_ctx, in_ctx2 => sys_keyval_api.co_dml_ctx, in_sub_ctx => sys_keyval_api.co_att_ctx).count > 0;
      --
      if l_att
      then
         l_&<name = "Alias" lowercase = "true" >_id_old := lr_&<name = "Alias" lowercase = "true" >_db.&<name = "Alias" lowercase = "true" >_id;
         sys_dml_events_api.set_event(in_det_code => co_det_code, in_operation => sys_dml_events_api.co_operation_update);
         cancel_private(in_id => l_&<name = "Alias" lowercase = "true" >_id_old, in_cancel_b => false, in_cancel_att => l_att);
         lr_&<name = "Alias" lowercase = "true" > := ins_private(in_&<name = "Alias" lowercase = "true" > => in_&<name = "Alias" lowercase = "true" >, in_ins_b => false, in_ins_att => l_att);
      end if;
      --
      lt_params.delete;
      append_par(io_params => lt_params, in_&<name = "Alias" lowercase = "true" > => lr_&<name = "Alias" lowercase = "true" >);
      logger.log(p_text => sys_log_constants.co_end, p_params => lt_params, p_scope => co_scope);
      --
      return lr_&<name = "Alias" lowercase = "true" >;
   exception
      when sys_exceptions.e_update_protection then
         logger.log(p_text => sys_log_constants.co_end, p_scope => co_scope);
         raise;
      when others then
         logger.log_error(p_text => sys_log_constants.co_exception, p_scope => co_scope,  p_params => lt_params);
         raise;
   end upd;

   /**
   * Cancels the given &<name = "Comment" >
   *
   * @param in_id &<name = "Comment"> to cancel (PK base record).
   */
   procedure cancel(in_id in integer) is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'cancel';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      logger.append_param(p_params => lt_params, p_name => 'in_id', p_val => in_id);
      logger.log(p_text => sys_log_constants.co_start, p_params => lt_params, p_scope => co_scope);
      --
      sys_dml_events_api.set_event(in_det_code => co_det_code, in_operation => sys_dml_events_api.co_operation_delete);
      cancel_private(in_id => in_id);
      --
      logger.log(p_text => sys_log_constants.co_end, p_scope => co_scope);
   exception
      when others then
         logger.log_error(p_text => sys_log_constants.co_exception, p_scope => co_scope, p_params => lt_params);
         raise;
   end cancel;

     
end  &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_tapi;
