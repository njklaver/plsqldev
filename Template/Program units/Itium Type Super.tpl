create or replace type &<Name = "Type Name"> force under &<Name = "Super Type" required = "Yes">
(
  /**
  * &<Name = "Purpose">
  */
)
/
create or replace type body &<Name = "Type Name"> is
  /*
  * &<Name = "Purpose">
  *
  * @author Nico Klaver -  nklaver@itium.nl
  */
end;
/
