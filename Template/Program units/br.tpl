create or replace package &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true">_br is
   /**
   * Business rules &<Name = "Base Name" lowercase = "true">
   */

   /**
   * Returns the package release
   * 
   * @return the package release
   */
   function release return sys_public_types.code_type;

   /**
   * Validates the &<Name = "Base Name" lowercase = "true"> supplied
   *
   * @param in_&<name = "alias" uppercase = "false" >           &<Name = "Base Name" lowercase = "true"> to handle
   * @param in_dml_operation DML operation (C, U, D) 
   * @param in_page_id       APEX page
   */
   procedure validate(in_app           in &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true">_v%rowtype
                     ,in_dml_operation in varchar2
                     ,in_page_id       in number);
end &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true">_br;
/
create or replace package body &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true">_br is
   /*
   * Business rules applications
   *
   * History
   * Date       Who         Release Jira           Comments
   * ---------- ----------- ------- -------------- --------------------------------------------------------------------------------
   * &<name="Date"> Nico Klaver &<Name = Release> &<Name = Jira> First creation       
   */
   /**
   * Release
   */
   co_release constant &<Name = "Prefix" lowercase = "true" required = "Yes">_public_types.code_type := '&<Name = Release>';

   /*
   * co_scope_prefix Logger scope prefix. 
   */
   co_scope_prefix constant &<Name = "Prefix" lowercase = "true" required = "Yes">_public_types.object_name_type := lower($$plsql_unit) || '.';

   /**
   * Returns the package release
   * 
   * @return the package release
   */
   function release return &<Name = "Prefix" lowercase = "true" required = "Yes">_public_types.code_type is
   begin
      return co_release;
   end release;

   /**
   * Validates the application supplied
   *
   * @param in_&<name = "alias" uppercase = "false" >           Application to handle
   * @param in_dml_operation DML operation (C, U, D) 
   * @param in_page_id       APEX page
   */
   procedure validate(in_&<name = "alias" uppercase = "false" >           in &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true">_v%rowtype
                     ,in_dml_operation in varchar2
                     ,in_page_id       in number) is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'validate';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true">_tapi.append_par(io_params => lt_params, in_&<name = "alias" uppercase = "false" > => in_&<name = "alias" uppercase = "false" >);
      logger.append_param(p_params => lt_params, p_name => &<Name = "Prefix" lowercase = "true" required = "Yes">_log_constants.co_in_page_id, p_val => in_page_id);
      logger.log(p_text => &<Name = "Prefix" lowercase = "true" required = "Yes">_log_constants.co_start, p_params => lt_params, p_scope => co_scope);
      --
      --
      if &<Name = "Prefix" lowercase = "true" required = "Yes">_message_api.has_errors
      then
         raise &<Name = "Prefix" lowercase = "true" required = "Yes">_exceptions.e_validation_failed;
      end if;
      --
      logger.log(p_text => &<Name = "Prefix" lowercase = "true" required = "Yes">_log_constants.co_end, p_scope => co_scope);
   exception
      when &<Name = "Prefix" lowercase = "true" required = "Yes">_exceptions.e_validation_failed then
         logger.log(p_text => &<Name = "Prefix" lowercase = "true" required = "Yes">_log_constants.co_end, p_scope => co_scope);
         raise;
      when others then
         logger.log_error(p_text => &<Name = "Prefix" lowercase = "true" required = "Yes">_log_constants.co_exception, p_scope => co_scope, p_params => lt_params);
         raise;
   end validate;
end &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true">_br;
/