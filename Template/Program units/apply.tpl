create or replace package &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_apply is
   /**
   * Apply API for &<Name = "Comment">
   */


   /**
   * Returns the package release
   * 
   * @return the package release
   */
   function release return sys_public_types.code_type;

   /**
   * Returns a new &<Name = "Comment"> record.
   *
   * @return  &<Name = "Comment"> record created.
   */
   function &<name = "alias" uppercase = "false" >() return &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype;

   /**
   * Apply &<name="comment"> to the database. 
   *
   * @param in_&<name = "alias" lowercase = "true" >    &<name = "comment"> to handle.
   * @param in_dml_operation DML operation (C, U, D)
   * @param in_page_id       APEX page (Optional)
   * @param out_id           PK &<name="comment"> (base record)
   */
   procedure apply(in_&<name = "alias" lowercase = "true" > in &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype
                  ,in_dml_operation in         varchar2
                  ,in_page_id       in         number default null
                  ,out_id           out nocopy number);

end  &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_apply;
/
create or replace package body &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_apply is
   /*
   * Apply API for &<Name = "Comment">
   *
   * History
   * Date       Who         Release Jira           Comments
   * ---------- ----------- ------- -------------- --------------------------------------------------------------------------------
   * &<name="Date"> Nico Klaver &<Name = Release> &<Name = Jira> First creation       
   */
   /**
   * Release
   */
   co_release constant sys_public_types.code_type := '1.0.0';

   /*
   * co_scope_prefix Logger scope prefix. 
   */
   co_scope_prefix constant sys_public_types.object_name_type := lower($$plsql_unit) || '.';

   /**
   * Returns the package release
   * 
   * @return the package release
   */
   function release return sys_public_types.code_type is
   begin
      return co_release;
   end release;   

   /**
   * Returns a new &<Name = "Comment"> record.
   *
   * @return  &<Name = "Comment"> record created.
   */
   function &<name = "alias" uppercase = "false" >() return &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype is
     --
     -- lr_&<name = "alias" uppercase = "false" > The &<Name = "Comment"> record handled.
     lr_&<name = "alias" uppercase = "false" > &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype;
   begin
      --
      return lr_&<name = "alias" uppercase = "false" >;
   end &<name = "alias" uppercase = "false" >;

   /**
   * APEX apply &<name="comment"> procedure. 
   *
   * @param in_&<name = "alias" lowercase = "true" > &<name = "comment"> to handle.
   * @param in_dml_operation DML operation 
   * @param in_page_id       APEX page (optional)
   * @param out_id           PK &<name="comment"> (base record)
   */
   procedure apply(in_&<name = "alias" lowercase = "true" > in &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype
                  ,in_dml_operation in         varchar2
                  ,in_page_id         in         number default null
                  ,out_id             out nocopy number) is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'apply_&<name = "alias" lowercase = "true" >';
      --
      -- lt_params_in Logger parameters.
      lt_params  logger.tab_param;
      --
      -- lr_&<name = "alias" lowercase = "true" > Record to handle
      lr_&<name = "alias" lowercase = "true" > &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_v%rowtype;
   begin
      logger.append_param(p_params => lt_params, p_name => sys_log_constants.co_in_dml_operation, p_val => in_dml_operation);
      logger.append_param(p_params => lt_params, p_name => sys_log_constants.co_in_page_id, p_val => in_page_id);
      &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_tapi.append_par(io_params => lt_params, in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >);
      logger.log(p_text => sys_log_constants.co_start, p_params => lt_params, p_scope => co_scope);
    --
    if in_page_id is not null
    then
      --
      -- Called from APEX set parent evnt
      sys_dml_events_api.set_parent(in_det_code => sys_dml_events_api.co_apex, in_process_name => co_scope, in_page_id => in_page_id);
    end if;
    --
    &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_br.validate(in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >
                                                                                                                                ,in_dml_operation => in_dml_operation
                                                                                                                                ,in_page_id => in_page_id);
    --
    case in_dml_operation
       when sys_apply_constants.co_create then
         lr_&<name = "alias" lowercase = "true" > := &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_tapi.ins(in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >);
         out_id := lr_&<name = "alias" lowercase = "true" >.&<name = "alias" lowercase = "true" >_id;
       when sys_apply_constants.co_update then
         lr_&<name = "alias" lowercase = "true" > := &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_tapi.upd(in_&<name = "alias" lowercase = "true" > => in_&<name = "alias" lowercase = "true" >);
         out_id := lr_&<name = "alias" lowercase = "true" >.&<name = "alias" lowercase = "true" >_id;
       when sys_apply_constants.co_delete then
         &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_tapi.cancel(in_id => in_&<name = "alias" lowercase = "true" >.&<name = "alias" lowercase = "true" >_id);
    end case;
    --
    lt_params.delete;
    &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_tapi.append_par(io_params => lt_params, in_&<name = "alias" lowercase = "true" > => lr_&<name = "alias" lowercase = "true" >);
    logger.log(p_text => sys_log_constants.co_end, p_params => lt_params, p_scope => co_scope);
  exception
      when sys_exceptions.e_update_protection then
         logger.log(p_text => sys_log_constants.co_update_protection, p_scope => co_scope);
         if in_page_id is not null
         then
            --
            -- Called from APEX show messages
            sys_message_api.flush_apex;
         else
            sys_message_api.flush;
            raise;
         end if;
      when sys_exceptions.e_validation_failed then
         logger.log(p_text => sys_log_constants.co_validation_failed, p_scope => co_scope);
         if in_page_id is not null
         then
            --
            -- Called from APEX show messages
            sys_message_api.flush_apex;
         else
            sys_message_api.flush;
            raise;
         end if;
      when others then
         logger.log_error(p_text => sys_log_constants.co_exception, p_scope => co_scope, p_params => lt_params);
         raise;
  end apply;
end  &<Name = "Prefix" lowercase = "true" required = "Yes">_&<Name = "Base Name" lowercase = "true" required = "Yes">_apply;
