create or replace type &<Name = "Type Name"> force as object
(
   /**
   * &<Name = "Purpose">
   */

   /**
   * Returns the type release
   * 
   * @return the type release
   */
   static function release return varchar2,
)
/
create or replace type body &<Name = "Type Name"> is
   /*
   * &<Name = "Purpose">
   *
   * Date       Who         Release Jira           Comments
   * ---------- ----------- ------- -------------- --------------------------------------------------------------------------------
   */
 
   /**
   * Returns the type release
   * 
   * @return the type release
   */
   static function release return varchar2 is
      --     
      -- Release
      co_release constant sys_public_types.code_type := '1.0.0';
   begin
      return co_release;
   end release;

end;
/
