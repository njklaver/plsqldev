create or replace package &<Name = "Package Name" lowercase = "true" required = "Yes"> is
   /**
   * &<Name = "Purpose">
   */


   /**
   * Returns the package release
   * 
   * @return the package release
   */
   function release return sys_public_types.code_type;
end &<Name = "Package Name" lowercase = "true" >;
/
create or replace package body &<Name = "Package Name" lowercase = "true" > is
   /*
   * &<Name = "Purpose">
   *
   * History
   * Date       Who         Release Jira           Comments
   * ---------- ----------- ------- -------------- --------------------------------------------------------------------------------
   * &<name = "Date"> Nico Klaver &<Name = Release> &<Name = Jira> First creation.
   */
   /**
   * Release
   */
   co_release constant sys_public_types.code_type := '1.0.0';

   /*
   * co_scope_prefix Logger scope prefix. 
   */
   co_scope_prefix constant sys_public_types.object_name_type := lower($$plsql_unit) || '.';


   /**
   * Returns the package release
   * 
   * @return the package release
   */
   function release return sys_public_types.code_type is
   begin
      return co_release;
   end release;   
end &<Name = "Package Name" lowercase = "true" >;
